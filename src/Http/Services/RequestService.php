<?php

namespace Emeka\Http\Services;

use GuzzleHttp\Client as Http;
use GuzzleHttp\Exception\RequestException;

class RequestService
{
	protected $wunderApi;

    /**
     * Creates an instance of RequestService
     */
    public function __construct(Http $http)
    {
		$this->http = $http;
		$this->wunderApi = getenv('API_URL');
	}

	/**
	 * {@inheritdoc}
	 */
	public function handle($method, $endpoint = null, $data = null)
	{
        try {
            $apiResponse = $this->http->request($method, $this->wunderApi . $endpoint, [
				'json' => $data,
                'headers' => [
                    'Content-Type' => 'application/json'
                ]
            ]);
			
			return [
				'apiDataBody' => json_decode($apiResponse->getBody()),
				'apiDataStatus' => $apiResponse->getStatusCode()
			];
        } 
        catch (ClientException $exception) 
        {
            Log::info($exception->getMessage());
            throw $exception;
        }
	}
}