<?php

namespace Emeka\Database;

use Emeka\Database\DatabaseConnection;
use Illuminate\Database\Capsule\Manager as Capsule;

class Schemes
{
    protected $capsule;

    function __construct()
    {
        $this->capsule = new Capsule();
        new DatabaseConnection($this->capsule);
    }

    /**
     * This method create users schema.
     */
    public function createUserTable()
    {

        if (! $this->capsule->schema()->hasTable('users')) 
        {
            $this->capsule->schema()->create('users', function ($table) {
                $table->increments('id');
                $table->string('account_owner');
                $table->string('first_name');
                $table->string('last_name');
                $table->string('phone_number');
                $table->string('address');
                $table->string('zip_code');
                $table->string('city');
                $table->string('iban');
                $table->string('house_number');
                $table->timestamps();
            });
            
            return "user table created";
        }
        else
        {
            return "user table already exists";
        }
    }

    /**
     * This method create users schema.
     */
    public function dropUserTable()
    {
        if ($this->capsule->schema()->hasTable('users')){
            $this->capsule->schema()->drop('users');
        }

        return "user table dropped";
    }

}