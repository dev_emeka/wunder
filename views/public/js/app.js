
$(document).ready(function() {
	
	var first_name = $('.first_name');
	var last_name = $('.last_name');
	var phone_number = $('.phone_number');
	var address = $('.address');
	var zip_code = $('.zip_code');
	var city = $('.city');
	var account_owner = $('.account_owner');
	var iban = $('.iban');
	var house_number = $('.house_number');

	iban.val(getCookie('iban'))
	city.val(getCookie('city'))
	address.val(getCookie('address'))
	zip_code.val(getCookie('zip_code'))
	last_name.val(getCookie('last_name'))
	first_name.val(getCookie('first_name'))
	house_number.val(getCookie('house_number'))
	phone_number.val(getCookie('phone_number'))
	account_owner.val(getCookie('account_owner'))


    $("#success_box").hide();

    $("#error_box").hide();

	function setCookie(name,value) {
		var days = 7;
		var expires = "";
		if (days) {
			var date = new Date();
			date.setTime(date.getTime() + (days*24*60*60*1000));
			expires = "; expires=" + date.toUTCString();
		}
		document.cookie = name + "=" + (value || "")  + expires + "; path=/";
	}

	function getCookie(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for(var i=0;i < ca.length;i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1,c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
		}
		return null;
	}

	function eraseCookie(name) {
		document.cookie = name+'=; Max-Age=-99999999;';
	}

});