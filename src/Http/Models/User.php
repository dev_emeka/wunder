<?php

namespace Emeka\Http\Models;

use Illuminate\Database\Eloquent\Model;


class User extends Model
{

	protected $table = 'users';

    protected $fillable = [
    	'last_name', 
  		'first_name',
  		'phone_number',
  		'zip_code',
  		'iban',
  		'house_number',
  		'account_owner',
  		'address',
  		'city',
	];
}