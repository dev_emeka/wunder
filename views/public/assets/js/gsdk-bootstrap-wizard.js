/*!

 =========================================================
 * Bootstrap Wizard - v1.1.1
 =========================================================
 
 * Product Page: https://www.creative-tim.com/product/bootstrap-wizard
 * Copyright 2017 Creative Tim (http://www.creative-tim.com)
 * Licensed under MIT (https://github.com/creativetimofficial/bootstrap-wizard/blob/master/LICENSE.md)
 
 =========================================================
 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 */

// Get Shit Done Kit Bootstrap Wizard Functions

searchVisible = 0;
transparent = true;

var first_name = $('.first_name');
var last_name = $('.last_name');
var phone_number = $('.phone_number');
var address = $('.address');
var zip_code = $('.zip_code');
var city = $('.city');
var account_owner = $('.account_owner');
var iban = $('.iban');
var house_number = $('.house_number');


var previous_button = $('.previous_button')
var next_button = $('.next_button')
var finish_button = $('.finish_button')

var resultAlert = $('.resultAlert')

var loadingAlert = $('.loadingAlert')
var resultAlertMsg = $('.resultAlertMsg')

var error_message = $('.error_message')
var errorAlert = $('.errorAlert')


iban.val(getCookie('iban'))
city.val(getCookie('city'))
address.val(getCookie('address'))
zip_code.val(getCookie('zip_code'))
last_name.val(getCookie('last_name'))
first_name.val(getCookie('first_name'))
house_number.val(getCookie('house_number'))
phone_number.val(getCookie('phone_number'))
account_owner.val(getCookie('account_owner'))

resultAlert.hide()
loadingAlert.hide()
errorAlert.hide()

$(document).ready(function(){

    /*  Activate the tooltips      */
    $('[rel="tooltip"]').tooltip();

    // Code for the Validator
    var $validator = $('.wizard-card form').validate({
		  rules: {
		    first_name: {
		      required: true,
		      minlength: 3
		    },
		    last_name: {
		      required: true,
		      minlength: 3
		    },
		    phone_number: {
		      required: true,
		      minlength: 3,
		    },
		    address: {
		      required: true,
		      minlength: 3,
		    },
		    house_number: {
		      required: true,
		      minlength: 3,
		    },
		    zip_code: {
		      required: true,
		      minlength: 3,
		    },
		    city: {
		      required: true,
		      minlength: 3,
		    },
		    account_owner: {
		      required: true,
		      minlength: 3,
		    },
		    iban: {
		      required: true,
		      minlength: 3,
		    },
        }
	});

    // Wizard Initialization
  	$('.wizard-card').bootstrapWizard({
        'tabClass': 'nav nav-pills',
        'nextSelector': '.btn-next',
        'previousSelector': '.btn-previous',

        onNext: function(tab, navigation, index) {

            console.log(tab, "tab")
            console.log(navigation, 'navigation')
            console.log(index, 'index')

        	var $valid = $('.wizard-card form').valid();
        	if(!$valid) {
        		$validator.focusInvalid();
        		return false;
            }
            
            saveInfo(index)
        },

        onInit : function(tab, navigation, index){

          //check number of tabs and fill the entire row
          var $total = navigation.find('li').length;
          $width = 100/$total;
          var $wizard = navigation.closest('.wizard-card');

          $display_width = $(document).width();

          if($display_width < 600 && $total > 3){
              $width = 50;
          }

           navigation.find('li').css('width',$width + '%');
           $first_li = navigation.find('li:first-child a').html();

           $moving_div = $('<div class="moving-tab">' + $first_li + '</div>');

           $('.wizard-card .wizard-navigation').append($moving_div);
           refreshAnimation($wizard, index);
           $('.moving-tab').css('transition','transform 0s');
       },

        onTabClick : function(tab, navigation, index){

            var $valid = $('.wizard-card form').valid();

            if(!$valid){
                return false;
            } else {
                return true;
            }
        },

        onTabShow: function(tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index+1;

            var $wizard = navigation.closest('.wizard-card');

            // If it's the last tab then hide the last button and show the finish instead
            if($current >= $total) {
                $($wizard).find('.btn-next').hide();
                $($wizard).find('.btn-finish').show();
            } else {
                $($wizard).find('.btn-next').show();
                $($wizard).find('.btn-finish').hide();
            }

            button_text = navigation.find('li:nth-child(' + $current + ') a').html();

            setTimeout(function(){
                $('.moving-tab').text(button_text);
            }, 150);

            var checkbox = $('.footer-checkbox');

            if( !index == 0 ){
                $(checkbox).css({
                    'opacity':'0',
                    'visibility':'hidden',
                    'position':'absolute'
                });
            } else {
                $(checkbox).css({
                    'opacity':'1',
                    'visibility':'visible'
                });
            }

            refreshAnimation($wizard, index);
        },

        onLast: function(tab, navigation, index){
            
        }
  	});

    $('[data-toggle="wizard-radio"]').click(function(){
        wizard = $(this).closest('.wizard-card');
        wizard.find('[data-toggle="wizard-radio"]').removeClass('active');
        $(this).addClass('active');
        $(wizard).find('[type="radio"]').removeAttr('checked');
        $(this).find('[type="radio"]').attr('checked','true');
    });

    $('[data-toggle="wizard-checkbox"]').click(function(){
        if( $(this).hasClass('active')){
            $(this).removeClass('active');
            $(this).find('[type="checkbox"]').removeAttr('checked');
        } else {
            $(this).addClass('active');
            $(this).find('[type="checkbox"]').attr('checked','true');
        }
    });

    $('.set-full-height').css('height', 'auto');


    $("#success_box").hide();

    $("#error_box").hide();

});

 //Function to show image before upload
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$(window).resize(function(){
    $('.wizard-card').each(function(){
        $wizard = $(this);
        index = $wizard.bootstrapWizard('currentIndex');
        refreshAnimation($wizard, index);

        $('.moving-tab').css({
            'transition': 'transform 0s'
        });
    });
});

function refreshAnimation($wizard, index){
    total_steps = $wizard.find('li').length;
    move_distance = $wizard.width() / total_steps;
    step_width = move_distance;
    move_distance *= index;

    $wizard.find('.moving-tab').css('width', step_width);
    $('.moving-tab').css({
        'transform':'translate3d(' + move_distance + 'px, 0, 0)',
        'transition': 'all 0.3s ease-out'

    });
}

function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		clearTimeout(timeout);
		timeout = setTimeout(function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		}, wait);
		if (immediate && !timeout) func.apply(context, args);
	};
};

function setCookie(name,value) {
    var days = 7;
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    document.cookie = name+'=; Max-Age=-99999999;';
}

function saveInfo(position) {

    switch (position){
        case 1:
            setCookie('first_name', first_name.val());
            setCookie('last_name', last_name.val());
            setCookie('phone_number', phone_number.val());

            break;
        case 2:
            setCookie('address', address.val());
            setCookie('house_number', house_number.val());
            setCookie('zip_code', zip_code.val());
            setCookie('city', city.val());

            break;
        case 3:
            setCookie('account_owner', account_owner.val());
            setCookie('iban', iban.val());
            
            loadingAlert.show()

            var postdata = $('#customer_info_form').serialize();

            var request = $.ajax({
                type: 'POST',
                url: '/api/create',
                data: postdata,
                dataType: 'json'
            });

            request.done(function( result ) {
                console.log(result);
                
                if(result.status === 400){
                    swal("Oops", "Account Owner Already Exists.", "error");
                    resultAlert.show();
                    resultAlertMsg.html('Account Owner Already Exists');
                    loadingAlert.hide()
                }
                else{
                    previous_button.hide()
                    finish_button.hide()
                    loadingAlert.hide()
                    resultAlert.show();
                    resultAlertMsg.html('You have registered you account successfully this is your account data Id: ' + result.payment_id);
                    
                    // empty all vars
                    emptyVars()
                }
            });

            break;
    }


    function emptyVars(){
        iban.val('')
        city.val('')
        address.val('')
        zip_code.val('')
        last_name.val('')
        first_name.val('')
        house_number.val('')
        phone_number.val('')
        account_owner.val('')

        setCookie('iban', '');
        setCookie('city', '');
        setCookie('account_owner', '');
        setCookie('address', '');
        setCookie('zip_code', '');
        setCookie('last_name', '');
        setCookie('first_name', '');
        setCookie('phone_number', '');
        setCookie('account_owner', '');
        setCookie('house_number', '');
    }
}
