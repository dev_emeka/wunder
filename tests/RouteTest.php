<?php

namespace Test;

use Faker\Factory;
use PHPUnit\Framework\TestCase;

use Emeka\Http\Models\User;
use Test\Http\Config\RequestService;

class RouteTest extends TestCase
{
	/**
	 * $faker
	 * @var Faker\Factory
	 */
	public $faker;

	/**
	 * $request
	 * @var Test\Http\Config\RequestService;
	 */
	public $request;

	public function setUp()
	{
	    parent::setUp();
	    $this->faker = Factory::create();
	    $this->request = new RequestService;
	}

	/**
	 * Test index route returns welcome message
	 */
	public function testIndexRoute()
	{
		$response = $this->request->handel('GET', 'http://localhost:1234/api', []);
		
	    $this->assertSame(200, $response->getStatusCode());
	    $this->assertSame('Wunder Mobility API', json_decode($response->getBody())->message);
	}

	/**
	 * Test index route returns welcome message
	 */
	public function testCreateRoute()
	{
		$userData = [
            'iban' => $this->faker->phoneNumber,
            'city' => $this->faker->city,
            'address' => $this->faker->streetAddress,
            'zip_code' => $this->faker->buildingNumber,
            'last_name' => $this->faker->lastName,
            'first_name' => $this->faker->firstName,
            'phone_number' => $this->faker->phoneNumber,
            'house_number' => $this->faker->buildingNumber,
            'account_owner' => $this->faker->firstName,
        ];

        $response = $this->request->handel('POST', 'http://localhost:1234/api/create', $userData);
        $this->assertIsObject($response->getBody());
        $this->assertSame(200, $response->getStatusCode());
	}
}