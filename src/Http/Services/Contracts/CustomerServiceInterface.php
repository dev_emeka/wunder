<?php

namespace Emeka\Http\Services\Contracts;

interface CustomerServiceInterface
{
	/**
	 * save customer to database
	 * @return json|null
	 */
	public function createCustomer($data);
	
	/**
	 * find customer from database
	 * @return json|null
	 */
	public function findBy($feild, $value);
}