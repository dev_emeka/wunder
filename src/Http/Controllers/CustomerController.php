<?php

namespace Emeka\Http\Controllers;

use Twig_Environment;
use Emeka\Http\Services\RequestService;
use Emeka\Http\Services\Contracts\CustomerServiceInterface;

/**
 * Class CustomerController
 * @package Emeka\Http\Controllers
 */
class CustomerController
{
	/**
	 * Twig_Environment
	 * @var Twig_Environment
	 */
	private $twig;

	/**
	 * CustomerService Service
	 * @var CustomerServiceInterface
	 */
	protected $customerServiceInterface;

	/**
	 * CustomerService Service
	 * @var RequestService
	 */
	protected $requestService;

	function __construct
	(
		Twig_Environment $twig,
		RequestService $requestService,
		CustomerServiceInterface $customerService
	)
	{
		$this->twig = $twig;
		$this->requestService = $requestService;
		$this->customerService = $customerService;
	}

	/**
	 * handle index request
	 * @return json|null
	 */
	public function index()
	{
		return $this->twig->render('index.twig');
	}

	/**
	 * handle index request
	 * @return json|null
	 */
	public function home()
	{
		return response()->httpCode(200)->json([
			"message" => "Wunder Mobility API",
			"status" => 200,
		]);
	}
	
	/**
	 * handle createCustomers request
	 * @return json|null
	 */
	public function create()
	{
		// check if Customer exist in the database
		$findUserByName = $this->customerService->findBy('account_owner', input()->all(['account_owner']))->get()->count();

		// return error is exist
		if ($findUserByName) {
			return response()->httpCode(200)->json([
				"message" => "Customer already exit",
				"status" => 400,
			]);
		}
		
		$customer = $this->customerService->createCustomer(input()->all());
		
		$response = $this->requestService->handle('POST', null, [
			'iban' => $customer['iban'],
			'customerId' => $customer['id'],
			'owner' => $customer['account_owner']
		]);
	
		if($response['apiDataStatus'] == 200){
			return response()->httpCode(200)->json([
				'payment_id' => $response['apiDataBody']->paymentDataId,
				'status' => 200
			]);
		}
		else 
		{
			return response()->httpCode(200)->json([
				"message" => "Payment Invalid try again",
				"status" => 400,
			]);
		}
	}
}