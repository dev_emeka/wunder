<?php

namespace Test\Http\Config;

use GuzzleHttp\Client;

class RequestService
{
	public $http;

	function __construct()
	{
		$this->http = new Client;		
	}

	public function handel($method, $url, $data = null)
	{
		return $this->http->request($method, $url, [
			'json' => $data,
			'headers' => [
				'Content-Type' => 'application/json'
			]
		]);
	}
}