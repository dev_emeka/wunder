<?php

/**
* This file contains all the routes for the project
*/
use Emeka\Router\RouterConfig as Router;
use Emeka\Middlewares\CsrfVerifier;
use Emeka\Middlewares\ApiVerification;
use Emeka\Http\Handlers\CustomExceptionHandler;

Router::csrfVerifier(new CsrfVerifier());

Router::group(['namespace' => '\Emeka\Http\Controllers', 'exceptionHandler' => CustomExceptionHandler::class], function () {

	Router::get('/', 'CustomerController@index');

	// API Routes
	Router::group(['prefix' => '/api'], function () {
		Router::get('/', 'CustomerController@home');
		Router::post('create', 'CustomerController@create');
	});
});