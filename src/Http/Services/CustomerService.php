<?php

namespace Emeka\Http\Services;

use Emeka\Http\Models\User;
use Emeka\Http\Services\Contracts\CustomerServiceInterface;

class CustomerService implements CustomerServiceInterface
{
	/**
	 * insert new customer into the database
	 * @param array $data
	 * @return json|null
	 */
	public function createCustomer($data)
	{
		return User::create($data);
	}

	/**
	 * insert new customer into the database
	 * @param array $data
	 * @return json|null
	 */
	public function findBy($feild, $value)
	{
		return User::where($feild, $value);
	}
}
